[![pipeline status](https://gitlab.com/Emilv2/huawei-solar/badges/master/pipeline.svg)](https://gitlab.com/Emilv2/huawei-solar/commits/master)
[![codecov](https://codecov.io/gl/Emilv2/huawei-solar/branch/master/graph/badge.svg)](https://codecov.io/gl/Emilv2/huawei-solar)
[![PyPI version](https://badge.fury.io/py/huawei-solar.svg)](https://badge.fury.io/py/huawei-solar)
[![PyPI - Python Version](https://img.shields.io/pypi/pyversions/huawei-solar.svg)](https://pypi.org/project/huawei-solar/)
[![PyPI - License](https://img.shields.io/pypi/l/huawei-solar.svg)](https://choosealicense.com/licenses/mit/)
[![Code style](https://img.shields.io/badge/code%20style-black-000000.svg)](https://github.com/python/black)

# local api for Huawei Inverters
